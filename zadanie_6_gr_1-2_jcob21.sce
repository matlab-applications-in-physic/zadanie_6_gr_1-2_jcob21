﻿// author  Jcob21 03.12.2019 
// script created in scilab 6.02 
// to run this program you need to add JSON toolbox to your scilab you can do it by following instructions on this page https://atoms.scilab.org/toolboxes/json

// this function makes program running forever  and repeat the code every 1 hour, the rest of this loop is at the end of code 
while True 


// saving the information in json from web 
        kat=JSONParse(mgetl(getURL('https://danepubliczne.imgw.pl/api/data/synop/id/12560')));
        cze=JSONParse(mgetl(getURL('https://danepubliczne.imgw.pl/api/data/synop/id/12550')));
        rac=JSONParse(mgetl(getURL('https://danepubliczne.imgw.pl/api/data/synop/id/12540')));




// changing the units from degrees to Fahrenheit to calculate the feels like temperature   city.information  is way to access essential values 
// we need to use strtod ( function that convert the string to numeric value (double type )) because we can't multiply or add number to string 
katTf=strtod(kat.temperatura)*9/5+32
czeTf=strtod(cze.temperatura)*9/5+32
racTf=strtod(rac.temperatura)*9/5+32


// the same as higher, converting the weend speed from KMH to MPH
katWm=strtod(kat.predkosc_wiatru)*0.6213
czeWm=strtod(cze.predkosc_wiatru)*0.6213
racWm=strtod(rac.predkosc_wiatru)*0.6213




function vFeelsLike=CityFeelsLikeTemp(vTemperature,vWindSpeed,vRelativeHumidity);


//This function calculates the feels like temperature 
// Try Wind Chill first


    if vTemperature <= 50 & vWindSpeed >= 3 then 


      vFeelsLike = 35.74 + (0.6215*vTemperature) - 35.75*(vWindSpeed**0.16) + ((0.4275*vTemperature)*(vWindSpeed**0.16));


    else


      vFeelsLike = vTemperature;
      
end 
    // Replace it with the Heat Index, if necessary


    if vFeelsLike == vTemperature & vTemperature >= 80 then 


      vFeelsLike = 0.5 * (vTemperature + 61.0 + ((vTemperature-68.0)*1.2) + (vRelativeHumidity*0.094));


    else if vFeelsLike >= 80 then 


        vFeelsLike = -42.379 + 2.04901523*vTemperature + 10.14333127*vRelativeHumidity - .22475541*vTemperature*vRelativeHumidity - .00683783*vTemperature*vTemperature - .05481717*vRelativeHumidity*vRelativeHumidity + .00122874*vTemperature*vTemperature*vRelativeHumidity + .00085282*vTemperature*vRelativeHumidity*vRelativeHumidity - .00000199*vTemperature*vTemperature*vRelativeHumidity*vRelativeHumidity;


        else if vRelativeHumidity < 13 & vTemperature >= 80 & vTemperature <= 112 then 


          vFeelsLike = vFeelsLike - ((13-vRelativeHumidity)/4)*math.sqrt((17-math.fabs(vTemperature-95.))/17);
            else  if vRelativeHumidity > 85 & vTemperature >= 80 & vTemperature <= 87 then 


            vFeelsLike = vFeelsLike + ((vRelativeHumidity-85)/10) * ((87-vTemperature)/5);
end
end
end
end
endfunction 
// now here we go back to metric system, and we execute functions for data from Cities 
katFeelsLike=(CityFeelsLikeTemp(katTf,katWm,strtod(kat.wilgotnosc_wzgledna))-32)*5/9;
czeFeelsLike=(CityFeelsLikeTemp(czeTf,czeWm,strtod(cze.wilgotnosc_wzgledna))-32)*5/9;
racFeelsLike=(CityFeelsLikeTemp(racTf,racWm,strtod(rac.wilgotnosc_wzgledna))-32)*5/9;




//Collecting all the data into one matrix for each city 
        katInfo=['katowice ','temperature: ',kat.temperatura,' celcius ','feels like temperature: ',string(katFeelsLike),' celcius ',' pressure: ',kat.cisnienie,' hectopascal ',' predkosc wiatru: ',kat.predkosc_wiatru,' meters per secons ',' wilgotnosc wzgledna: ',kat.wilgotnosc_wzgledna,'% ','time: ',kat.godzina_pomiaru+':00']
        czeInfo=['czestochowa ','temperature: ',cze.temperatura,' celcius ','feels like temperature: ',string(czeFeelsLike),' celcius ',' pressure: ',cze.cisnienie,' hectopascal ',' predkosc wiatru: ',cze.predkosc_wiatru,' meters per secons ',' wilgotnosc wzgledna: ',cze.wilgotnosc_wzgledna,'% ','time: ',cze.godzina_pomiaru+'00']
        racInfo=['raciboz ','temperature: ',rac.temperatura,' celcius ','feels like temperature: ',string(racFeelsLike),' celcius ',' pressure: ',rac.cisnienie,' hectopascal ',' predkosc wiatru: ',rac.predkosc_wiatru,' meters per secons ',' wilgotnosc wzgledna: ',rac.wilgotnosc_wzgledna,'% ','time: ',rac.godzina_pomiaru+':00']


// saving the data into Csv file , the last argument is header 
Pogoda= mopen('weatherData_kat.csv','a')
csvWrite(katInfo,' weatherData_kat.csv',';','.',[],'city;1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17')
mclose('all')


Pogoda= mopen('weatherData_cze.csv','a')
csvWrite(czeInfo,' weatherData_cze.csv',';','.',[],'city;1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17')
mclose('all')


Pogoda= mopen('weatherData_rac.csv','a')
csvWrite(racInfo,' weatherData_rac.csv',';','.',[],'city;1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17')
mclose('all')






// the rest of the loop its at the end of the program because, nobody wants to wait 1 hour for results when they start program 
sleep(3600,"s")
end